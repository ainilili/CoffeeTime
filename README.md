### 介绍
此项目是NF框架的演示项目，同时也是第一个试水项目，NF将会在这个项目中体现它的功能，并不断的优化创新！
### 项目配置
```src/main/java``` Java文件存放目录

```src/main/resource``` 配置文件存放目录

```web```   前端页面存放目录
### 项目搭建

```git clone https://gitee.com/ainilili/CoffeeTime.git```

> 以Maven项目的模式导入你的IDE中

### Host修改
默认Host都是本地链接，也可将Host改成```47.94.103.241```直连笔者的云服务器！